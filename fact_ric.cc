#include <iostream>
#include <iomanip>

using namespace std;

long double fact(int);

int main() 
{
	cout << "Numero: ";
	int n;
	cin >> n;
	cout << "Fattoriale: " << setprecision(30)<< fact(n) << endl;
	return 0;
}

long double fact(int n)
{
	long double toRet;
	if(n==0)	toRet=1;
	else		toRet=n*fact(n-1);
	return toRet;
}
