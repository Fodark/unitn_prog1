#include<iostream>
#include<cstdlib>
#include<cstring>
#include<fstream>

using namespace std;

const int MAX_SIZE=56;
bool encrypt=false;

void createMap(char[], char[]);
void printArray(const char[], const int);
void parseArgs(int, char*[]);
void printUsage();

int main(int argc, char* argv[])
{
   parseArgs(argc, argv);

   char tableFrom[MAX_SIZE]={};
   char tableTo[MAX_SIZE]={};
   createMap(tableFrom, tableTo);

   fstream input;
   input.open(argv[2], ios::in);
   if(input==NULL)
   {
      cout << "Errore apertura file!" << endl;
      exit(1);
   }

   fstream output;
   output.open(argv[3], ios::out);

   char c;
   if(encrypt)
      while(input.get(c))
      {
         int i=0;
         bool found=false;
         for(;i<MAX_SIZE && !found;i++)
            if(c==tableFrom[i])
               found=true;
            if(found)
               output.put(tableTo[i-1]);
            else
               output.put(c);
      }
   else
      while(input.get(c))
      {
         int i=0;
         bool found=false;
         for(;i<MAX_SIZE && !found;i++)
            if(c==tableTo[i])
               found=true;
            if(found)
               output.put(tableFrom[i-1]);
            else
               output.put(c);
      }
   output.close();
   input.close();
}

void createMap(char from[], char to[])
{
   fstream tab;
   tab.open("encrTable", ios::in);
   char c;
   int i=0, j=0;
   while(tab >> c)
   {
      if(i==j)
      {
         from[i]=c;
         i++;
      }
      else
      {
         to[j]=c;
         j++;
      }
   }
   from[MAX_SIZE-1]=' ';
   to[MAX_SIZE-1]='<';
   tab.close();
}

void printArray(const char arr[], const int dim)
{
   cout << "{";
   for(int i=0; i < dim; i++)
   {
      cout << arr[i] << ", ";
   }
   cout << "}" << endl;
}

void parseArgs(int argc, char* argv[])
{
   switch(argc)
   {
      case 4:
         if(*argv[1]=='E' || *argv[1]=='e')
            encrypt=true;
         else if(*argv[1]=='D' || *argv[1]=='d')
            encrypt=false;
         else
         {
            cout << "Errore nell'opzione specificata!";
            printUsage();
            exit(1);
         }
         break;
      default:
         printUsage();
         exit(0);
         break;
   }
}

void printUsage()
{
   cout  << endl << endl << "Encrypt/Decrypt Project | Usage:" << endl
         << "exec [E e | D d] source destination" << endl
         << "E e\tEncrypt the \'source\' file to \'destination\'" << endl
         << "D d\tDecrypt the \'source\' file to \'destination\'" << endl << endl;
}
