#include <iostream>

using namespace std;

void prova(const int& z)
{
	int* p=&z;
	*p=100;
}

int main()
{
	int x=5;
	cout << x << endl;
	prova(x);
	cout << x << endl;
	return 0;
}
