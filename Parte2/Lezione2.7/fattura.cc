#include "fattura.h"
#include <iostream>
using namespace std;

void stampa(Fattura& f)
{
   cout << "Fattura in data: " << f.data.giorno << "/" << f.data.mese << "/" << f.data.anno << endl;
   cout << "Nome cliente: " << f.cliente.nome << " " << f.cliente.cognome << endl;
   cout << "Articolo: " << f.articolo << endl;
   cout << "Pagata: " << f.pagata;
}
