#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

const int NUM_SPECIE=4;

enum Specie {FAGGIO, PINO, QUERCIA, BETULLA};

struct Albero
{
   Specie nome;
   int numero;
};

void riempiGiardino(Albero***, int, int, int[]);
void stampaGiardino(Albero***, int, int);
Albero*** preparaGiardino(int, int);
void sradica(Albero***, int, int);


int main(int argc, char* argv[])
{
  Albero*** giardino;
  int quanti[]={3,1,2,1,3};
  int dimX=0, dimY=0;
  do {
    cout << "Dimensione X (>=9): ";
    cin >> dimX;
  } while(dimX < 9);

  do {
    cout << "Dimensione Y (>=9): ";
    cin >> dimY;
  } while(dimY < 9);

  giardino=preparaGiardino(dimX, dimY);
  riempiGiardino(giardino, dimX, dimY, quanti);
  stampaGiardino(giardino, dimX, dimY);
  sradica(giardino, dimX, dimY);
  return (0);
}

void stampaGiardino(Albero*** g, int x, int y)
{
  for(int i=0; i < x; i++)
  {
    for(int j=0; j < y; j++)
    {
      if(g[i][j] == NULL)
        cout << "* ";
      else
        switch(g[i][j]->nome)
        {
          case FAGGIO: cout << "f "; break;
          case PINO: cout << "p "; break;
          case QUERCIA: cout << "q "; break;
          case BETULLA: cout << "b "; break;
        }
    }
    cout << endl;
  }
}

Albero*** preparaGiardino(int x, int y)
{
  Albero*** giardino=new Albero**[x];

  for(int i=0; i < x; i++)
  {
    giardino[i]=new Albero* [y];
  }

  for(int i=0; i < x; i++)
  {
    for(int j=0; j < y; j++)
    {
      giardino[i][j]=NULL;
    }
  }

  return giardino;
}

void riempiGiardino(Albero*** giardino, int x, int y, int quanti[])
{
  srand(time(NULL));
  for(int i=0; i < NUM_SPECIE; i++)
  {
    for(int j=0; j < quanti[i]; j++)
    {
      int xrand;
      int yrand;

      do {
        xrand=rand()%x;
        yrand=rand()%y;
      } while(giardino[xrand][yrand]!=NULL);

      giardino[xrand][yrand]=new Albero;
      giardino[xrand][yrand]->nome=(Specie) i;
      giardino[xrand][yrand]->numero=j+1;
    }
  }
}

void sradica(Albero*** giardino, int x, int y)
{
  for(int i=0; i < x; i++)
  {
    for(int j=0; j < y; j++)
    {
      delete giardino[i][j];
    }
    delete[] giardino[i];
  }
  delete[] giardino;
}
