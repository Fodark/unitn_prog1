#ifndef FATTURA_H
#define FATTURA_H

const int LUN=100;

struct Data
{
   int giorno, mese, anno;
};

struct Cliente
{
   char nome[LUN];
   char cognome[LUN];
};

struct Fattura
{
   Data data;
   bool pagata;
   Cliente cliente;
   char articolo[LUN];
   int quantita;
};

void stampa(Fattura&);

#endif
