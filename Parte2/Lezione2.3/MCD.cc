// INPUT: mese, giorno, anno
// PROCESSO: correzione con flag

//MCD(x,y) ricorsivo x>y
// Se y==0 MCD =x
// MCD(x,y)=MCD(y,x%y)

int mcd(int, int);

#include <iostream>
using namespace std;

int main()
{
  int x, y;
  cout << "x: ";
  cin >> x;
  cout << "y: ";
  cin >> y;
  int minimo=mcd(x, y);
  cout << "MCD(" << x << ", " << y <<") = " << minimo << endl;
  return (0);
}

int mcd(int x, int y)
{
  int toReturn;
  if(y==0) toReturn = x;
  else toReturn = mcd(y, x%y);
  return toReturn;
}
