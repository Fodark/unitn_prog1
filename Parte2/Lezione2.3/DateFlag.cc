// INPUT: mese, giorno, anno
// PROCESSO: correzione con flag

//MCD(x,y) ricorsivo x>y
// Se y==0 MCD =x
// MCD(x,y)=MCD(y,x%y)

void estrai(int, int&, int&, int&, int&);

#include <iostream>
using namespace std;

int main()
{
  int data, gg, mm, aaaa, flag;
  cout << "Data (GGMMAAAA): ";
  cin >> data;
  estrai(data, gg, mm, aaaa, flag);
  cout << gg << "/" << mm << "/" << aaaa << (flag ? " con " : " senza ") <<"modifiche!"<<endl;
  return (0);
}

void estrai(int data, int& gg, int& mm, int& aaaa, int& flag)
{
  flag=0;
  bool bisestile=0;
  aaaa=data%10000;
  data/=10000;
  mm=data%100;
  data/=100;
  gg=data;

  if(aaaa < 0)
  {
    aaaa=0;
    flag=1;
  }
  bisestile=((aaaa%4==0) && (aaaa%400==0));
  if(mm <=0 || mm > 12)
  {
    mm=12;
    flag=1;
  }

  if(gg <=0 || gg > 31)
  {
    gg=31;
    flag=1;
  }

  switch(mm)
  {
    case 2:
      if(bisestile && gg > 29) { gg=29; flag=1; }
      else if(gg > 28) { gg=28; flag=1; }
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      if(gg > 30) { gg=30; flag=1; }
      break;
    default:
      if(gg > 31) { gg = 31; flag = 1;}
  }

}
