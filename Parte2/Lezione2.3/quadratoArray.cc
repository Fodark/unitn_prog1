#include <iostream>
#include <iomanip>
using namespace std;

void quadrato(int[], int);
void print(const int[], int);

const int DIM=5;

int main()
{
  int a[DIM]={2,4,6,7,8};
  print(a, DIM);
  quadrato(a, DIM);
  print(a, DIM);
  return (0);
}

void quadrato(int a[], int dim)
{
  for(int i=0; i < dim; i++)
  {
    a[i] *= a[i];
  }
}

void print(const int a[], int dim)
{
  for(int i=0; i< dim; i++)
    cout << "[" << i << "]=" << setw(3) << a[i] << "; ";
  cout << endl;
}
