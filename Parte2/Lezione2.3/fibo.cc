#include <iostream>
using namespace std;

//T(0)=0; T(1)= T(2) = 1;   T(n)=T(n-1) + T(n-2) + T(n-3)

long tribonacci(int);

int main()
{
  int n;
  cout << "Inserire n: ";
  cin >> n;
  cout << "tribonacci(" << n << "): " << tribonacci(n) << endl;
  return (0);
}

long tribonacci(int n)
{
  long toReturn;
  switch(n)
  {
    case 0:
      toReturn=0; break;
    case 1:
    case 2:
      toReturn=1; break;
    default:
      toReturn=tribonacci(n-1) + tribonacci(n-2) + tribonacci(n-3); break;
  }

  return toReturn;
}
