#include <iostream>
using namespace std;

const int DIM=5;

bool checkEqual(const char[], const char[], int dim);
void copy(const char[], char[]);
void print(const char[], int dim);

int main()
{
  char a[]={'a','b','c','d','e'};
  char b[DIM];
  cout << "a: ";
  print(a, DIM);
  cout << "b: ";
  print(b, DIM);
  if(!checkEqual(a,b, DIM))
    copy(a, b);

  cout << "a: ";
  print(a, DIM);
  cout << "b: ";
  print(b, DIM);

  return (0);
}

bool checkEqual(const char a[], const char b[], int dim)
{
  bool equal=1;
  for(int i=0; i < dim && equal; i++)
  {
    equal = equal && (a[i] == b[i]);
  }

  return equal;
}

void copy(const char source[], char destination[])
{
  for(int i=0; i < DIM; i++)
  {
    destination[i]=source[i];
  }
}

void print(const char a[], int dim)
{
  for(int i=0; i< dim; i++)
    cout << "[" << i << "]=" << a[i] << "; ";
  cout << endl;
}
