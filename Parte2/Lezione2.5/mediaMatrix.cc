#include <iostream>
using namespace std;

const int MAX_DIM=100;

double media(const int[MAX_DIM][MAX_DIM], const int);
double media(const int[MAX_DIM][MAX_DIM], const int, const int, const int);
double somma(const int[], const int, const int);
void print(const int[][MAX_DIM], const int dim);

int main()
{
  int dim=3;
  int matrix[MAX_DIM][MAX_DIM]={{1,2,3},{4,5,6},{7,8,9}};
  print(matrix, dim);
  cout << "Media: " << media(matrix, dim) << endl;
  return (0);
}

double media(const int matrix[MAX_DIM][MAX_DIM], const int dim)
{
   double toReturn=0.0;
   toReturn=media(matrix,0,dim-1,dim)/(dim*dim);
   return toReturn;
}

double media(const int matrix[MAX_DIM][MAX_DIM], const int inizio, const int fine, const int dim)
{
  double toReturn;
  if(inizio==fine)
    toReturn=somma(matrix[inizio], 0, dim-1);
  else
    toReturn=somma(matrix[inizio], 0, dim-1)+media(matrix, inizio+1, fine, dim);
  return toReturn;
}

double somma(const int arr[], const int inizio, const int fine)
{
  double toReturn;
  if(inizio==fine)
    toReturn=arr[inizio];
  else
    toReturn=arr[inizio]+somma(arr, inizio+1, fine);
  cout << "Somma["<<inizio<<"]="<<toReturn << endl;
  return toReturn;
}

void print(const int m[][MAX_DIM], const int dim)
{
  for(int i=0; i < dim; i++)
  {
    for (int j = 0; j < dim; j++)
      cout << m[i][j] << "\t";
    cout << endl;
  }
  cout << endl;
}
