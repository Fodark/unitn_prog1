#include <iostream>
using namespace std;
const int MAX_DIM=100;

void estraiDiagonale(const int[][MAX_DIM], int*, const int);
void print(const int[], const int dim);
void print(const int[][MAX_DIM], const int dim);

int main()
{
  int dim=3;
  int matrix[MAX_DIM][MAX_DIM]={{1,2,3},{4,5,6},{7,8,9}};
  print(matrix, dim);
  int diagonal[dim]={};
  estraiDiagonale(matrix, diagonal, dim);
  print(diagonal, dim);
  return (0);
}

void estraiDiagonale(const int matrix[][MAX_DIM], int* diagonal, const int dim)
{
   for(int i=0; i < dim; i++, diagonal++)
   {
      *diagonal=matrix[i][i];
   }
}

void print(const int a[], const int dim)
{
  for(int i=0; i< dim; i++)
    cout << "diag[" << i << "]=" << a[i] << "; ";
  cout << endl;
}

void print(const int m[][MAX_DIM], const int dim)
{
  for(int i=0; i < dim; i++)
  {
    for (int j = 0; j < dim; j++)
      cout << m[i][j] << " ";
    cout << endl;
  }
  cout << endl;
}
