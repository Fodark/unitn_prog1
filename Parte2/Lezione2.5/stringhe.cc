#include <iostream>
#include <cctype>
using namespace std;

const int MAX_DIM=256;
void codifica(char[], const char[]);
char caesar(const char);

int main()
{
  char c;
  char sorgente[MAX_DIM], destinazione[MAX_DIM];
  int i=0;
  cin.getline(sorgente, MAX_DIM);
  codifica(destinazione, sorgente);
  cout << destinazione << endl;
  return (0);
}

void codifica(char dest[], const char sorg[])
{
  for (; *sorg!='\0'; sorg++, dest++) {
    *dest=caesar(*sorg);
  }
  *dest='\0';
}

char caesar(const char c)
{
  char toReturn;
  if(isalpha(c))
    if(islower(c))
      toReturn=(c-'a'+3)%26 +'a';
    else if(isupper(c))
      toReturn=(c-'A'+3)%26 +'A';
    else
      toReturn=(c-'0'+3)%10 + '0';
  else
    toReturn=c;
  return toReturn;
}
