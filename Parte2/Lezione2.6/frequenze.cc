#include <iostream>
#include <cstring>
#include <cstdlib>

using namespace std;

const int SIZE = 100;

void stampaArray(int a[], int dim);
int* mescolaArray(int a[], int b[], int dim_a, int dim_b);
int* leggiArray(int&);

void stampaAst(int);

int main(int argc, char* argv[])
{
  if(argc < 2)
  {
     cout << "Error" << endl;
     exit(-1);
  }
  int* arr;
  int l=0;
  int max=0;
  for(int i=1; i < argc; i++)
  {
     if(strlen(argv[i]) > max)
      max=strlen(argv[i]);
  }

  arr=new int[max];
  for(int i=0; i < max; i++)
  {
     arr[i]=0;
  }
  for(int i=1; i < argc; i++)
  {
     arr[strlen(argv[i])-1]++;
  }
  stampaArray(arr, max);
  return 0;
}

void stampaArray(int a[], int dim) {
  for(int i = 0; i < dim; i++) {
    cout << i+1 << ": ";
    stampaAst(a[i]);
    cout << endl;
  }
  cout << endl;
}

void stampaAst(int l)
{
   for(int i = 0; i < l; i++) {
     cout << "#";
   }
}
