#include <iostream>
#include <cstring>
#include <cstdlib>

using namespace std;

const int LUNGHEZZA = 256;

char* codifica(const char t[]);
char caesar(char c);

int main(int argc, char* argv[]) {
  char c, sorgente[LUNGHEZZA], *destinazione;
  cout << "Introdurre stringa da codificare: ";
  int i = 0;
  while (cin.get(c) && i < LUNGHEZZA - 1) {
    sorgente[i] = c;
    i++;
  }
  sorgente[i] = '\0';
  destinazione=codifica(sorgente);
  cout << "Stringa codificata: " << destinazione << endl;
  return 0;
}

char* codifica(const char t[]) {
  int i, lun = strlen(t);
  char* s=new char[lun+1];
  for(i = 0; i < lun; i++) {
    s[i] = caesar(t[i]);
  }
  s[i] = '\0';
  return s;
}

char caesar(char c) {
  /*
     Alternativamente (versione compatta):
     return (isalpha(c) ? (islower(c) ?
                             (c - 'a' + 3) % 26 + 'a' :
                             (c - 'A' + 3) % 26 + 'A'
                           ) : c;
  */
  char r = c;
  if(isalpha(c)) {
    if(islower(c)) {
      r = (c - 'a' + 3) % 26 + 'a';
    } else {
      r = (c - 'A' + 3) % 26 + 'A';
    }
  }
    return r;
}
