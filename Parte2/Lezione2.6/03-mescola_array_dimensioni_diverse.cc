#include <iostream>

using namespace std;

const int SIZE = 100;

void stampaArray(int a[], int dim);
int* mescolaArray(int a[], int b[], int dim_a, int dim_b);
int* leggiArray(int&);

int main() {
  int* a;
  int* b;
  int* c;
  int n, m;
  a=leggiArray(n);
  b=leggiArray(m);
  c=mescolaArray(a, b, n, m);
  stampaArray(c, n + m);
  return 0;
}

int min(int a, int b) {
  return a < b ? a : b;
}

int* leggiArray(int& dim) {
  cout << "Dimensione: ";
  cin >> dim;
  int* toReturn=new int[dim];
  for(int i = 0; i < dim; i++) {
    cout << i << ": ";
    cin >> toReturn[i];
  }
  return toReturn;
}

void stampaArray(int a[], int dim) {
  for(int i = 0; i < dim; i++) {
    cout << a[i] << " ";
  }
  cout << endl;
}

int* mescolaArray(int a[], int b[], int dim_a, int dim_b) {
  // Notare la dichiarazione fuori dal ciclo for
  int i;
  // Si puo' rendere piu' efficiente calcolando
  // una volta sola "min(dim_a, dim_b)"
  int* toReturn=new int[dim_a+dim_b];
  for(i = 0; i < min(dim_a, dim_b) * 2; i++) {
    if(i % 2 == 0) {
      toReturn[i] = a[i / 2];
    } else {
      toReturn[i] = b[(i - 1) / 2];
    }
  }
  // Completo, con gli elementi rimasti
  for(; i < dim_a + dim_b; i++) {
    if(dim_a > dim_b) {
      toReturn[i] = a[i - min(dim_a, dim_b)];
    } else {
      toReturn[i] = b[i - min(dim_a, dim_b)];
    }
  }
  return toReturn;
}
