using namespace std;
#include <iostream>
#include <fstream>
#include <cstdlib>

int main (int argc, char * argv[])
{
  fstream myin,myout;

  if (argc!=3) {
    cout << "Usage: ./a.out <sourcefile> <targetfile>\n";
    exit(0);
  }

  myin.open(argv[1],ios::in);
  if (myin.fail()) {
     cerr << "Il file " << argv[1] << " non esiste\n";
     exit(0);
  }

  myout.open(argv[2],ios::out);

  float n;
  char buffer[256];

  while (myin >> n)
  {
    if(n>=100)
      myout << (n*1.02);
    else if (n>=50)
      myout << (n*1.05);
    else
      myout << (n*1.08);
    myout << " ";
  }
  myin.close();
  myout << endl;
  myout.close();
  return 0;
}
