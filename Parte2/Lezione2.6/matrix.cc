using namespace std;
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>

const char DELIM=',';
const int DIM=4;

void stampaM(const int[DIM][DIM], const int);

int main (int argc, char * argv[])
{
  fstream myin;
  int m[DIM][DIM];

  if (argc!=2) {
    cout << "Usage: ./a.out <sourcefile> \n";
    exit(0);
  }

  myin.open(argv[1],ios::in);
  if (myin.fail()) {
     cerr << "Il file " << argv[1] << " non esiste\n";
     exit(0);
  }

  int n;
  char c;
  for(int i=0; i < DIM; i++)
  {
    for(int j=0; j < DIM; j++)
    {
      n=0;
      while (myin.get(c) && c !=',')
      {
        n= ((n*10)+ (c-'0'));
      }
      m[i][j]=n;
    }
  }
  stampaM(m, DIM);
  myin.close();
  return 0;
}

void stampaM(const int matrix[DIM][DIM], const int dim)
{
  for(int i=0; i < dim; i++)
  {
    for(int j=0; j < dim; j++)
    {
      cout << setw(5) << matrix[i][j];
    }
    cout << endl;
  }
}
