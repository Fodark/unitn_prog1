#include <iostream>
using namespace std;

int max(int, int);

int main()
{
  int i, j;
  cout << "Dammi due interi: ";
  cin >> i >> j;
  cout << "Il massimo vale: " << max(i, j) << endl;
  return (0);
}

int max(int a, int b)
{
  int max= a > b ? a : b;
  return max;
}
