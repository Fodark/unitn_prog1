#include <iostream>
using namespace std;

void estrai(int, int&, int&, int&);

int main()
{
  int data, gg, mm, aaaa;
  cout << "Data (GGMMAAAA): ";
  cin >> data;
  estrai(data, gg, mm, aaaa);
  cout << gg << "/" << mm << "/" << aaaa << endl;
  return (0);
}

void estrai(int data, int& gg, int& mm, int& aaaa)
{
  aaaa=data%10000;
  data/=10000;
  mm=data%100;
  data/=100;
  gg=data;
}
