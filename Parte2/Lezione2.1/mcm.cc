#include <iostream>
using namespace std;

int mcm(int, int);
int massimo(int, int);

int main()
{
  int i, j;
  cout << "Dammi due interi: ";
  cin >> i >> j;
  cout << "M.C.M. : " << mcm(i, j) << endl;
  return (0);
}

int mcm(int num1, int num2)
{
  int mcm = 0;
  if(num1>0 && num2>0)
  {
    mcm=num1*num2;
    int max=massimo(num1, num2);
    for(int i = mcm; i >= max; i--)
    {
      if(i%num1==0 && i%num2==0)
      {
        mcm = i;
      }
    }
  }

  return mcm;
}

int massimo(int a, int b)
{
  int max= a > b ? a : b;
  return max;
}
