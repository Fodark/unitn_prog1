#include <iostream>
using namespace std;

void division(int, int, int&, int&);

int main()
{
  int dvd, dvs, quoziente, resto;
  cout << "Dividendo: ";
  cin >> dvd;
  cout << "Divisore: ";
  cin >> dvs;

  division(dvd, dvs, quoziente, resto);
  cout << "La divisone vale: " << quoziente << " con resto: " << resto << endl;
  return (0);
}

void division(int dvd, int dvs, int& quoziente, int& resto)
{
  quoziente=dvd/dvs;
  resto=dvd%dvs;
}
