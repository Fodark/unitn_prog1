#include <iostream>
using namespace std;

int pot(int);

int main()
{
  int i;
  cout << "Numero: ";
  cin >> i;
  cout << "2 ^ " << i << " = " << pot(i) << endl;
  return (0);
}

int pot(int i)
{
  int toReturn;
  if(i==0) toReturn=1;
  else toReturn=2*pot(i-1);
  return toReturn;
}
