#include <iostream>
#include <cmath>
using namespace std;

double radq(const double*, bool*);

int main()
{
  double num;
  bool negativo;
  cout << "Inserisci un numero: ";
  cin >> num;

  double r=radq(&num, &negativo);
  if(negativo)
  {
    cout << "Valore negativo!" << endl;
  }
  else
  {
    cout << "La radice vale: " << r << endl;
  }

  return (0);
}

double radq(const double* n, bool* negativo)
{
  double toReturn;
  if(*n >= 0)
  {
    *negativo=false;
    toReturn=sqrt(*n);
  }
  else
  {
    *negativo=true;
    toReturn=0;
  }
  return toReturn;
}
