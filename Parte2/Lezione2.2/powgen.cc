#include <iostream>
using namespace std;

int pot(int, int);

int main()
{
  int i, b;
  cout << "Base: ";
  cin >> b;
  cout << "Exp: ";
  cin >> i;
  cout << b <<" ^ " << i << " = " << pot(b, i) << endl;
  return (0);
}

int pot(int b, int i)
{
  int toReturn;
  if(i<=0) toReturn=1;
  else toReturn=b*pot(b, i-1);
  return toReturn;
}
