#include <iostream>
#include <cmath>
using namespace std;

void sphere(double, bool*, double*, double*);

int main()
{
  double num, vol, sup;
  bool negativo;
  cout << "Inserisci un numero: ";
  cin >> num;

  sphere(num, &negativo, &vol, &sup);
  if(negativo)
  {
    cout << "Raggio negativo!" << endl;
  }
  else
  {
    cout << "Volume: " << vol << endl
         << "Superficie: " << sup << endl;
  }

  return (0);
}

void sphere(double r, bool* negativo, double* vol, double* sup)
{
  if(r >= 0)
  {
    *negativo=false;
    *vol=M_PI*4/3*pow(r, 3);
    *sup=M_PI*4*pow(r, 2);
  }
  else
  {
    *negativo=true;
  }
}
