#include <iostream>
#include <iomanip>
using namespace std;

void inverti(int[], int);
void inverti1(int a[], int index, int dim);
void print(const int[], int);
void leggi(int[], int&);
void swap(int&, int&);

const int DIM=1000;

int main()
{
  int a[DIM]={};
  int n;
  leggi(a, n);
  inverti(a, n);
  print(a, n);
  return (0);
}

void print(const int a[], int dim)
{
  for(int i=0; i< dim; i++)
    cout << "a[" << i << "]=" << setw(3) << a[i] << "; ";
  cout << endl;
}

void leggi(int a[], int& dim)
{
  cout << "dim: ";
  cin >> dim;
  for(int i=0; i< dim; i++)
  {
    cout << "a["<< i << "]: ";
    cin >> a[i];
  }

  cout << endl;
}

void inverti(int a[], int dim)
{
  inverti1(a, 0, dim);
}

void inverti1(int a[], int index, int dim)
{
  if(dim < 2)
    return;
  else
  {
    swap(a[index], a[index+dim-1]);
    inverti1(a, index+1, dim-2);
  }

}

void swap(int& a, int& b)
{
  int t=a;
  a=b;
  b=t;
}
