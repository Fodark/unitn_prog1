#include <iostream>
#include <iomanip>
using namespace std;

void stampaInverti(const int[], int);
void print(const int[], int);
void leggi(int[], int);

const int DIM=1000;

int main()
{
  int a[DIM]={};
  int n;
  cout << "dim: ";
  cin >> n;
  leggi(a, n);
  stampaInverti(a, n);
  return (0);
}



void print(const int a[], int dim)
{
  for(int i=0; i< dim; i++)
    cout << "a[" << i << "]=" << setw(3) << a[i] << "; ";
  cout << endl;
}

void leggi(int a[], int dim)
{
  for(int i=0; i< dim; i++)
  {
    cout << "a["<< i << "]: ";
    cin >> a[i];
  }

  cout << endl;
}

void stampaInverti(const int a[], int dim)
{
  if(dim > 0)
  {
    cout << a[dim-1] << " ";
    stampaInverti(a, dim-1);
  }
}

void swap(int& a, int& b)
{
  int t=a;
  a=b;
  b=t;
}
