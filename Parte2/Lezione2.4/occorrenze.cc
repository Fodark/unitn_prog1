#include <iostream>
using namespace std;

enum ESITO{V, N, P};
const int DIM=100;

int conta(const ESITO, const ESITO[], const int);

int main()
{
  ESITO partite[DIM]={V,N,N,V,N,P,N,P,V,V,V,P,V,N,N,V};
  int dim=16;

  cout << "Numero vittorie: " << conta(V, partite, dim) << endl;
  cout << "Numero pareggi: " << conta(N, partite, dim) << endl;
  cout << "Numero sconfitte: " << conta(P, partite, dim) << endl;

  return (0);
}

int conta(const ESITO e, const ESITO partite[], const int dim)
{
  int toReturn=0;
  
  if(dim >=1)
    toReturn= (partite[dim-1]==e) + conta(e, partite, dim-1);

  return toReturn;
}
