#include <iostream>
#include <iomanip>
using namespace std;

void inverti(int[], int);
void print(const int[], int);
void leggi(int[], int);
void swap(int&, int&);

const int DIM=1000;

int main()
{
  int a[DIM]={};
  int n;
  cout << "dim: ";
  cin >> n;
  leggi(a, n);
  inverti(a, n);
  print(a, n);
  return (0);
}



void print(const int a[], int dim)
{
  for(int i=0; i< dim; i++)
    cout << "a[" << i << "]=" << setw(3) << a[i] << "; ";
  cout << endl;
}

void leggi(int a[], int dim)
{
  for(int i=0; i< dim; i++)
  {
    cout << "a["<< i << "]: ";
    cin >> a[i];
  }

  cout << endl;
}

void inverti(int a[], int dim)
{
  for(int i=0; i<dim/2; i++)
  {
    //cout << a[i] << " " << a[dim-1-i] << endl;
    swap(a[i], a[dim-1-i]);
  }

}

void swap(int& a, int& b)
{
  int t=a;
  a=b;
  b=t;
}
