#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

int count(char*);

int main(int argc, char** argv)
{
	if(argc != 3)
	{
		cout << "Error, exit code: 1" << endl;
		exit(1);
	}

	fstream input;
	input.open(argv[1], ios::in);

	if(input == NULL)
	{
		cout << "Error, exit code: 2" << endl;
		exit(2);
	}

	fstream output;
	output.open(argv[2], ios::out);

	char tmp[81];

	while(input >> tmp)
	{
		output << count(tmp) << " ";
	}

	output << endl;
	output.close();
	input.close();

	return 0;
}


int count(char* a)
{
	int toRet = 0;
	for(int i = 0; a[i] != '\0'; i++)
	{
		if(a[i] == 'a')
			toRet++;
	}
	return toRet;
}

