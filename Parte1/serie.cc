#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int main()
{
  int n;
  cout << "Inserire il numero di iterazioni da eseguire: ";
  cin >> n;

  double somma=0.0;
  for(int i=0; i < n; i++)
  {
   somma+=pow(2.0, -i);
  }
  cout << "Somma: " <<setprecision(20)<< somma<<endl;
  return (0);
}
