#include <iostream>
using namespace std;

int main()
{
  int a, i=0;
  int partial=0, fibo=1, last=0;
  char cont='s';

  cout << "Inserisci un intero: ";
  cin >> a;

  while(i<a)
  {

    partial=last;             //Il nuovo parziale e' l'ultimo valore di fibonacci
    cout << fibo << " ";

    last=fibo;                //Prima della modifica di fibo, salvo il suo valore come "ultimo"
    fibo+=partial;            //Calcolo il nuovo valore di fibo
    i++;
  }

  cout << endl;
  return (0);
}
