#include <iostream>
using namespace std;

int main()
{
  long num, ndivisori=2;
  cout << "Dammi un intero: ";
  cin >> num;

  for(long i=2;(i<=num/2)&&(ndivisori==2);i++)
  {
    cout << i << endl;
    if(num%i==0)
      ndivisori++;
  }

  if(ndivisori==2 && num!=1)
    cout << num << " e' primo!" << endl;
  else
    cout << num << " non e' primo!" << endl;

  return (0);
}
