#include <iostream>
#include <iomanip>
using namespace std;

const int diff=('a'-'A');
const int aMaiusc='A';
const int zMaiusc='Z';

int main()
{  
  for(int i=aMaiusc;i<=zMaiusc;i++)
  {
    cout <<  "| " << (char) i << " | " << setw(3) << i << " |\t| " << (char) (i+diff) << " | " << setw(3) << (i+diff) << " |" << endl;
  }
  return (0);
}
