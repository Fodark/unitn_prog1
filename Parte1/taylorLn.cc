#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int main()
{
  float x=-2.0f;
  double logaritmo=0.0;
  int passi=0;

  do
  {
    cout << "Inserire x per calcolare ln(1+x): ";
    cin >> x;
  } while(fabs(x)>1.0f);

  do
  {
    cout << "Inserire il numero massimo di passi: ";
    cin >> passi;
  } while(passi<1);

  for(int n=1; n<=passi; n++)
  {
    logaritmo+= (pow(-1.0,n+1)*((pow(x,n)/n)));
  }

  cout << "Il logaritmo calcolato con la serie di Taylor vale: " << "\t" << setprecision(30)<< logaritmo << endl;
  cout << "Il logaritmo calcolato con cmath vale: " << "\t\t\t" << setprecision(30) << log(1+x) << endl;


  return (0);
}
