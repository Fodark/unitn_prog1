#include <iostream>
using namespace std;

int main()
{
  int base, altezza;

  cout << "Inserisci base: ";
  cin >> base;
  cout << "Inserisci altezza: ";
  cin >> altezza;

  float area=base*altezza/2.0;

  cout << "Area: "<< area << endl;
  return (0);
}
