#include <iostream>
using namespace std;

int main()
{
  int base, expo, power=1;
  cout << "Dammi la base: ";
  cin >> base;
  cout << "Dammi l'esponente: ";
  cin >> expo;
  for(int i=0;i<expo;i++)
  {
    power*=base;
  }

  cout << "La potenza vale: " << power << endl;

  return (0);
}
