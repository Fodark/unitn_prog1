#include <iostream>
using namespace std;

bool isPrime(long num)
{
  long ndivisori=2;

  for(long i=2;(i<=num/2)&&(ndivisori==2);i++)
  {
    //cout << i << endl;
    if(num%i==0)
      ndivisori++;
  }

  if(ndivisori==2 && num!=1)
    return true;
  else
    return false;
}


int main()
{
  long num;
  int ndivisori=0;
  cout << "Dammi un intero: ";
  cin >> num;

  for(long i=num;i>=1;i--)
  {
    if(num%i==0) {
      ndivisori++;
      if(isPrime(i))
        cout << i << " | ";
    }
  }
  cout << endl;
  return (0);
}
