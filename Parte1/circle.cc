#include <iostream>
#include<cmath>
using namespace std;

int main()
{
  int radius=30;
  double angle, coseno;

  for (int i = 0; i <= 2*radius; i++) {
    angle=M_PI/2 - (M_PI/2/radius*i);
    coseno=cos(angle);
    int shift = (int) (coseno*radius);

    if(i==0 || i==2*radius)
    {
      cout << '\r';
      for(int j=0;j<2*radius;j++)
        cout << ' ';
      cout << "*\v";
    }
    else
    {
      for(int j=0; j<2*shift;j++)
        cout << ' ';
      cout << '*';

      for(int j=0; j<=4*shift+1;j++)
        cout << '\b';
      cout << '*';

      for(int j=0;j<2*shift;j++)
        cout << ' ';
      cout << '\v';
    }
  }
  cout << '\r';
  return (0);
}
