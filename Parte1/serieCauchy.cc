#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int main()
{
  double eps;
  cout << "Inserire precisione richiesta: ";
  cin >> eps;

  int i;

  double somma=1.0;
  double ultimasomma=0.0;
  for(i=1; fabs(somma-ultimasomma) >= fabs(eps*somma); i++)
  {
   ultimasomma=somma;
   somma+=pow(2.0, -i);
  }
  cout << "Somma: " <<setprecision(30)<< somma<<endl << "N giri: " << i <<endl;
  return (0);
}
