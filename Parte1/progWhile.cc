#include <iostream>
using namespace std;

int main()
{
  int a=0, i=0, max=0;
  char cont='s';

  while (cont=='s')
  {
    cout << "Inserisci un valore intero: ";
    cin >> a;

    if(i==0 || a > max)
      max=a;

    i++;

    cout << "Vuoi inserire un altro? [s/n]: ";
    cin >> cont;
  }

  cout << "Il massimo vale: " << max << endl;
  return (0);
}
