#include <iostream>
using namespace std;

int main()
{
  char c;
  cout << "Dammi un carattere: ";
  cin >> c;

  if((c>='a' && c<='z')||(c>='A'&&c<='Z'))
    {
      cout << "Il carattere inserito e' alfabetico!" << endl;
      if(c>='a')
	{
	  c-=('a'-'A');
	}
      
      if(c=='A' || c=='E' || c=='I' || c=='O' || c=='U')
	{
	  cout << "Hai inserito una vocale!" << endl;
	}
      else
	{
	  cout << "Hai inserito una consonante!" << endl;
	}
    }
  else
    {
      cout << "Carattere NON alfabetico!" << endl;
    }
  
  return (0);
}
