#include <iostream>
using namespace std;

int main()
{
  // int num1, num2, confronto;
  //cout << "Inserisci primo numero: ";
  //cin >> num1;
  //cout << "Inserisci secondo numero: ";
  //cin >> num2;

  //confronto=num1==num2;
  cout << "\tTabella di verita' operatore AND"<< endl
       << "a \t b \ta&&b" << endl
       << "0 \t 0 \t"<<(0&&0)<<endl
       << "0 \t 1 \t"<<(0&&1)<<endl
       << "1 \t 0 \t"<<(1&&0)<<endl
       << "1 \t 1 \t"<<(1&&1)<<endl<<endl;

  cout << "\tTabella di verita' operatore OR"<< endl
       << "a \t b \ta||b" << endl
       << "0 \t 0 \t"<<(0||0)<<endl
       << "0 \t 1 \t"<<(0||1)<<endl
       << "1 \t 0 \t"<<(1||0)<<endl
       << "1 \t 1 \t"<<(1||1)<<endl<<endl;

  cout << "\tTabella di verita' operatore NOT"<< endl
       << "a \t!a" << endl
       << "0 \t"<<(!0)<<endl
       << "1 \t"<<(!1)<<endl<<endl;
  return (0);
}
