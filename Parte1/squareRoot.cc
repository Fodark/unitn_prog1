#include <iostream>
#include <cmath>
using namespace std;

int main()
{
  double a, b, c, delta, radice1, radice2;
  cout << "Inserire il coefficiente a dell\'equazione ax^2+bx+c=0: ";
  cin >> a;
  cout << "Inserire il coefficiente b dell\'equazione ax^2+bx+c=0: ";
  cin >> b;
  cout << "Inserire il coefficiente c dell\'equazione ax^2+bx+c=0: ";
  cin >> c;

  if(a!=0)
    {
  
    delta=pow(b,2)-(4*a*c);
    if(delta<0)
      {
	cout << "Due soluzioni nel campo complesso:" << endl
	     << "Prima soluzione: " << -b <<"-"<<sqrt(-delta)<<"i" << endl
	     << "Seconda soluzione: " << -b <<"+"<<sqrt(-delta)<<"i" << endl;
      }

    else if(delta==0)
      {
	cout << "Due soluzioni reali e coincidenti:" << endl
	     << "Soluzione: " << (-b-sqrt(delta))/(2*a) << endl;			     
      }
    else
      {
	radice1=(-b-sqrt(delta))/(2*a);
	radice2=(-b+sqrt(delta))/(2*a);
	cout << "Due soluzioni reali:" << endl
	     << "Prima soluzione: " << radice1 << endl
	     << "Seconda soluzione: " << radice2 << endl;
      }
    
    }
  else
    {
      cout << "La a non puo' essere 0!" << endl;
    }
  return (0);
}
