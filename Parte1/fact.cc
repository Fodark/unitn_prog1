#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  int num;
  cout <<"Dammi un numero intero: ";
  cin >> num;
  long double fact=1.0;

  for(int i=2;i<=num;i++)
  {
    fact*=i;
  }
  cout << "Il fattoriale vale: " <<setprecision(30)<< fact << endl;

  return (0);
}
