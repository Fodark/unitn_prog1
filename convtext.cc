#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>

const int DIFF = 'a' - 'A';

using namespace std;

int main(int argc, char** argv)
{
	if(argc != 3)
	{
		cout << "Errore" << endl;
		exit(1);
	}
	
	fstream input;
	input.open(argv[1], ios::in);
	if(input == NULL)
	{
		cout << "Errore" << endl;
		exit(2);
	}

	fstream output;
	output.open(argv[2], ios::out);

	char tmp[31];
	input >> tmp;
	if(tmp[0] >= 'a' && tmp[0] <= 'z')
		tmp[0]-=DIFF;

	
	
	bool flag=false;
	while(!input.eof())
	{
		if(flag && tmp[0] >= 'a' && tmp[0] <= 'z')
			tmp[0]-=DIFF;
		flag = false;
		output << tmp << " ";
		int lunghezza = strlen(tmp);
		if(tmp[lunghezza - 1] == '.' || tmp[lunghezza - 1] == ':' || tmp[lunghezza - 1] == '?')
			flag=true;
		input >> tmp;
	}
	output << endl;
	output.close();
	input.close();
	
	return 0;
}

