#include <iostream>

using namespace std;

const int MAX = 10;

int* merge(int*, int*);
void stampa(int*);

int main(int argc, char** argv)
{
	int a[MAX] = {1,3,6,7,8,9,10,15,17,18};
	int b[MAX] = {2,4,5,11,12,13,14,16,19,20};
	int* c = merge(a,b);
	stampa(c);
}

int* merge(int* a, int* b)
{
	int* ret = new int[2*MAX];
	int i=0, j=0, k=0;

	while(i < 2*MAX)
	{
		if(j >= MAX)
		{
			ret[i] = b[k];
			i++;
			k++;
		}
		else if(k >= MAX)
		{
			ret[i] = a[j];
			i++;
			j++;
		}
		else
		{
			if(a[j] <= b[k])
			{
				ret[i] = a[j];
				i++;
				j++;
			}
			else
			{
				ret[i] = b[k];
				i++;
				k++;
			}
		}
	
	}
	
	return ret;
}

void stampa(int* arr)
{
	for(int i = 0; i < 2*MAX; i++)
	{
		cout << "[" << i << "]: " << arr[i] << endl;
	}
}

