#include <iostream>
#include <cstdlib>

using namespace std;

int euclide(int, int);

int main(int argc, char** argv)
{
	if(argc != 3)
	{
		cout << "Error!" << endl;
		exit(1);
	}

	cout << "Result: " << euclide(atoi(argv[1]), atoi(argv[2])) << endl;

	return 0;
}

int euclide(int a, int b)
{
	int toRet;
	if(b != 0)
		toRet = euclide(b, a%b);
	else
		toRet = a;
	return toRet;
}
