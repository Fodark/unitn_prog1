#include "stack.h"
#include <iostream>

using namespace std;

void init(stack* s)
{
	s->head = NULL;
}

RET push(stack* s, int n)
{
	RET toRet;
	node tmp = new (nothrow) element;
	if(tmp == NULL)
		toRet = ERROR;
	else
	{
		tmp->value = n;
		tmp->next = s->head->next;
		s->head = tmp;
		toRet = OK;
	}
	return toRet;
}

RET pop(stack* s)
{
	RET toRet;
	if(empty(s))
		toRet = ERROR;
	else
	{
		node tmp = s->head;
		s->head = s->head->next;
		delete tmp;
		toRet = OK;
	}
	return toRet;
}

RET top(stack* s, int& n)
{
	RET toRet;
	if(empty(s))
		toRet = ERROR;
	else
	{
		n = s->head->value;
		toRet = OK;
	}
	return toRet;
}

RET empty(stack* s)
{
	return (s->head == NULL ? OK : ERROR);
}

