#ifndef STACK_H
#define STACK_H

enum RET{ERROR, OK};

struct element
{
	int value;
	element* next;
};

typedef element* node;

struct stack
{
	node head;
};

void init(stack*);
RET push(stack*, int);
RET pop(stack*);
RET top(stack*, int&);
RET empty(stack*);

#endif
