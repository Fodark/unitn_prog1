#include <iostream>
#include <iomanip>
using namespace std;

double pow2(int);

int main()
{
  int n; double approx=0.0;
  cout << "Numero iterazioni?: ";
  cin >> n;

  for(int i=0;i<n;i++)
  {
    approx+=1/pow2(i);
  }
  cout << "Valore: " << setprecision(20) << approx << endl;
  return (0);
}

double pow2(int i)
{
  double toReturn=1.0;
  for(int j=1;j<=i;j++)
  {
    toReturn=toReturn*2.0;
    //cerr << "toR: " << toReturn << endl;
  }
  return toReturn;
}
