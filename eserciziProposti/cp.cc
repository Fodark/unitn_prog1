#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
using namespace std;

int main(int argc, char* argv[])
{
  fstream in, out;
  char c;
  if(argc!=3)
  {
    cout << "cp command simulation" << endl << "Usage: ./a.out source destination" << endl;
    exit(0);
  }
  in.open(argv[1], ios::in);
  if(in==NULL)
  {
    cout << "Impossibile aprire il file sorgente" << endl;
    exit(0);
  }

  out.open(argv[2], ios::out);

  while(in.get(c))
    out.put(c);

  return (0);
}
