#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int throw3Dices();

int main()
{
  srand(time(NULL));
  int attacc, defensor;
  attacc=throw3Dices();
  defensor=throw3Dices();
  cout << (attacc > defensor ? "Attaccante" : "Difensore") << " vince!" << endl;
  return (0);
}

int throw3Dices()
{
  int toReturn=0;
  for(int i=0;i<3;i++)
  {
    toReturn+= (int) (rand()%6+1);
    cerr << toReturn << endl;
  }
  return toReturn;
}
