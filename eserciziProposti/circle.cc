#include <iostream>
#include <cmath>
using namespace std;

int areaC(double raggio, double& area, double& perimetro);

int main()
{
  double raggio, area, perimetro;
  cout << "Inserire raggio: ";
  cin >> raggio;
  if(areaC(raggio, area, perimetro))
  {
    cout << "L'area vale: " << area << endl
         << "Il perimetro vale: " << perimetro << endl;
  }
  else
  {
    cout << "Calcolo impossibile, raggio < 0" << endl;
  }


  return (0);
}

int areaC(double raggio, double& area, double& perimetro)
{
  int toReturn=0;
  if(raggio>=0)
  {
    area=raggio*raggio*M_PI;
    perimetro=2*raggio*M_PI;
    toReturn=1;
  }
  return toReturn;

}
