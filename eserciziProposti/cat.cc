#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
using namespace std;

int main(int argc, char* argv[])
{
  fstream in;
  char c;
  if(argc!=2)
  {
    cout << "cat command simulation" << endl << "Usage: ./a.out source" << endl;
    exit(0);
  }
  in.open(argv[1], ios::in);
  if(in==NULL)
  {
    cout << "Impossibile aprire il file sorgente" << endl;
    exit(0);
  }

  while(in.get(c))
  {
     cout << c;
  }
  cout << endl;
  return (0);
}
