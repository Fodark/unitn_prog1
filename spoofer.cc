#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

int main(int argc, char** argv)
{
	if(argc!=2)
	{
		cout << "Usage: source input_file" << endl;
		exit(1);
	}

	fstream input;
	input.open(argv[1], ios::in);
	
	if(input==NULL)
	{
		cout << "Errore durante l'apertura del file" << endl;
		exit(2);
	}

	char c;
	int n_char = 0;
	int n_substring = 0;
	int n_string = 0;

	while(input.get(c))
	{
		
		if(c>='0' && c <='9')
			n_char++;

		if(n_char == 4)
		{
			n_substring++;
			n_char = 0;
		}

		if(n_substring == 4)
		{
			n_string++;
			n_char = 0;
			n_substring = 0;
		}
	}
	
	input.close();
	cout << "Numero di stringhe idonee: " << n_string << endl;

	return 0;
}
