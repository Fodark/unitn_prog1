#ifndef QUEUE_H
#define QUEUE_H

enum RET{ERROR, OK};

struct element
{
	int value;
	element* next;
};

typedef element* node;

struct queue
{
	node head;
	node tail;
};

void init(queue*);
RET enqueue(queue*, int);
RET dequeue(queue*);
RET first(queue*, int&);
RET empty(queue*);

#endif
