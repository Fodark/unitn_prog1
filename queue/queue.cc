#include "queue.h"
#include <iostream>

using namespace std;

void init(queue* q)
{
	q->head = q->tail = NULL;
}

RET enqueue(queue* q, int n)
{
	RET toRet;
	node tmp = new (nothrow) element;
	if(tmp == NULL)
		toRet = ERROR;
	else
	{
		tmp->value = n;
		if(empty(q))
			q->head = tmp;
		else
			q->tail->next = tmp;
		q->tail = tmp;
		toRet = OK;
	}
	return toRet;
}

RET dequeue(queue* q)
{
	RET toRet;
	if(empty(q))
		toRet = ERROR;
	else
	{
		node tmp = q->head;
		q->head = q->head->next;
		delete tmp;
	}
	return toRet;
}

RET first(queue* q, int& n)
{
	RET toRet;
	if(empty(q))
		toRet = ERROR;
	else
	{
		n = q->head->value;
	}
	return toRet;
}

RET empty(queue* q)
{
	return (q->head == NULL ? ERROR : OK);
}
