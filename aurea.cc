using namespace std;
#include <iostream>
#include <iomanip>

long long f (int n) 
{
  long long res;
  
  if (n==0||n==1) 
    res = 1;
  else
    res = f(n-1)+f(n-2);

  return res;
}

int main() 
{
  for(int i=1; i< 100; i++) {
	cout << "Giro: " << i << " approx: " <<setprecision(20) <<(float)f(i)/f(i-1) << endl;
	}
return 0;
}

